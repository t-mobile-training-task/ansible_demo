#!/bin/bash
BACKUP_NAME="blog" 
DB_NAME="{{ mysql_db  }}" 
DB_USER="{{mysql_user }}" 
DB_PASS="{{ mysql_password }}"
 
mysqldump --no-tablespaces -u $DB_USER -p$DB_PASS $DB_NAME | gzip > /backup/backup_name_$(date +%Y-%m-%d_%H-%M-%S).sql.gz
 
find /backup -type f -mtime +30 -exec rm -f {} \;
